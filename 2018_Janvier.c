#define MAGIC 0xCAFEBABE
#define SECTOR_SIZE 100
#define NB_DIRECT (SECTOR_SIZE / (4 * sizeof(int)))
#define BLOC_NULL -1
#define TIMER_CLOCK 69
#define TIMER_PARAM 69
#define TIMER_ALARM 69
#define TIMER_IRQ 2

enum file_type_e
{
    ordinary,
    directory,
    special
};

extern int current_volume;
void read_bloc(int vol, int bloc, char *buffer);
void read_bloc_n(int vol, int bloc, char *buffer, int size);

void write_bloc(int vol, int bloc, char *buffer);
void write_bloc_n(int vol, int bloc, char *buffer, int size);

int new_bloc();
void free_bloc(int bloc);

//Question 01 : structure file_entry
// ###############################################################
struct file_entry
{
    char filename[32];
    int file_number;
};

// QUestion 02 : Pourquoi pas de char* type * ?
// ###############################################################
// On y écrirait alors les pointeurs vers ces valeurs (qui se trouvent dans la mémoire et donc non-persistées).
// De plus, même si on écrit les valeurs présentes dans la mémoire en disque, rien ne nous garantit qu'on dépasse la taille maximale (de 32 char)

// Question 03 : structure du directory
// ###############################################################
#define NB_ENTRIES (SECTOR_SIZE - 3 * sizeof(int)) // la taille d'un secteur - la taille des champs magic, type et next
struct inode_dir_s
{
    unsigned int magic;
    enum file_type_e type;
    struct file_entry entries[NB_ENTRIES];
    int next;
};

// Question 4 : createDirectory
// ###############################################################
int createDirectory();

int createDirectory()
{
    int bloc;
    int i;
    struct inode_dir_s newDir;

    bloc = new_bloc();
    if (bloc == BLOC_NULL)
    { // gestion erreur
        return -1;
    }

    // initialiser le new directory
    newDir.magic = MAGIC;
    newDir.type = directory;
    for (i = 0; i < NB_ENTRIES; i++)
    {
        newDir.entries[i].filename = "";
        newDir.entries[i].file_number = BLOC_NULL;
    }
    newDir.next = BLOC_NULL;

    // Ecrire la structure dans le bloc alloué
    write_bloc_n(current_volume, bloc, (char *)&newDir, sizeof(struct inode_dir_s));

    return bloc;
}

// Question 5 : removeDirectory
// #################################################################
void removeDirectory(int inumber_dir)
{
    struct inode_dir_s dir;
    int bloc;

    // Lire le directory
    read_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));

    // checks
    assert(dir.magic == MAGIC);
    assert(dir.type == directory);

    // liberation
    bloc = dir.next;
    free_bloc(inumber_dir);

    while (bloc != BLOC_NULL)
    {
        read_bloc_n(current_volume, bloc, (char *)&dir, sizeof(struct inode_dir_s));
        assert(dir.magic == MAGIC);
        assert(dir.type == special);
        inumber_dir = bloc;
        bloc = dir.next;
        free_bloc(inumber_dir);
    }
}

// Question 06 : addEntry
// ################################################################
void addEntry(int inumber_dir, char *filename, int file_number);

void addEntry(int inumber_dir, char *filename, int file_number)
{
    struct inode_dir_s dir;
    int i;
    int bloc;

    // assert
    assert(strlen(filename) <= 32);

    // lire directory
    read_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));
    assert(dir.magic == MAGIC);
    assert(dir.type == directory);

    // chercher entree libre
    for (i = 0; i < NB_ENTRIES; i++)
    {
        // entree libre trouvee
        if (strcmp(dir.entries[i].filename, "") == 0)
        {
            strcpy(dir.entries[i].filename, filename);
            dir.entries[i].file_number = file_number;
            write_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));
            return;
        }
    }

    // Ici, je n'ai pas d'entrée dans dir
    bloc = dir.next;
    while (bloc != BLOC_NULL)
    {
        // lire le fragment de dir
        read_bloc_n(current_volume, bloc, (char *)&dir, sizeof(struct inode_dir_s));
        assert(dir.magic == MAGIC);
        assert(dir.type == special);

        inumber_dir = bloc; // retenir le bloc du directory lu

        // chercher entree libre
        for (i = 0; i < NB_ENTRIES; i++)
        {
            // entree libre trouvee
            if (strcmp(dir.entries[i].filename, "") == 0)
            {
                strcpy(dir.entries[i].filename, filename);
                dir.entries[i].file_number = file_number;
                write_bloc_n(current_volume, bloc, (char *)&dir, sizeof(struct inode_dir_s));
                return;
            }
        }

        // passer au bloc suivant
        bloc = dir.next;
    }

    // Si on arrive ici, c'est qu'on n'a pas trouvé d'entrée libre et que le chainage est full
    // il faut creer un nouveau chainage
    bloc = new_bloc();
    assert(bloc != BLOC_NULL);

    dir.next = bloc;                                                                     // on a encore le dernier fragment dans la variable dir, on rajoute le next
    write_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s)); // sauvegarder le dernier fragment avec le nouveau next

    // Initialiser la structure du nouveau fragment
    dir.magic = MAGIC;
    dir.type = special;
    dir.next = BLOC_NULL;
    // entrees vides
    for (i = 0; i < NB_ENTRIES; i++)
    {
        dir.entries[i].file_number = -1;
        strcpy(dir.entries[0].filename, "");
    }
    // ajouterl 'entree
    dir.entries[0].file_number = file_number;
    strcpy(dir.entries[0].filename, filename);

    // sauvegarder le nouveau fragment
    write_bloc_n(current_volume, bloc, (char *)&dir, sizeof(struct inode_dir_s));
}

// Question 07 : rmEntry
// ####################################################
int rmEntry(int inumber_dir, char *filename);
int rmEntry(int inumber_dir, char *filename)
{
    struct inode_dir_s dir;
    int i;

    // lire directory
    read_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));
    assert(dir.magic == MAGIC);
    assert(dir.type == directory);
    for (i = 0; i < NB_ENTRIES; i++)
    {
        // entry trouvée
        if (strcmp(dir.entries[i].filename, filename) == 0)
        {
            strcpy(dir.entries[i].filename, "");
            write_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));
            return dir.entries[i].file_number;
        }
    }
    // Si je suis ici, c'est que je n'ai pas trouvé dans le premier bloc de type directory

    inumber_dir = dir.next;
    while (inumber_dir != BLOC_NULL)
    {
        // lire directory
        read_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));
        assert(dir.magic == MAGIC);
        assert(dir.type == special);

        for (i = 0; i < NB_ENTRIES; i++)
        {
            // entry trouvée
            if (strcmp(dir.entries[i].filename, filename) == 0)
            {
                strcpy(dir.entries[i].filename, "");
                write_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));
                return dir.entries[i].file_number;
            }
        }
    }

    return 0;
}

// Question 08 : findEntry
// ################################################
int findEntry(int inumber_dir, char *filename);
int findEntry(int inumber_dir, char *filename)
{
    struct inode_dir_s dir;
    int i;

    // lire directory
    read_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));
    assert(dir.magic == MAGIC);
    assert(dir.type == directory);
    for (i = 0; i < NB_ENTRIES; i++)
    {
        // entry trouvée
        if (strcmp(dir.entries[i].filename, filename) == 0)
        {
            return dir.entries[i].file_number;
        }
    }
    // Si je suis ici, c'est que je n'ai pas trouvé dans le premier bloc de type directory

    inumber_dir = dir.next;
    while (inumber_dir != BLOC_NULL)
    {
        // lire directory
        read_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));
        assert(dir.magic == MAGIC);
        assert(dir.type == special);

        for (i = 0; i < NB_ENTRIES; i++)
        {
            // entry trouvée
            if (strcmp(dir.entries[i].filename, filename) == 0)
            {
                return dir.entries[i].file_number;
            }
        }
    }

    return BLOC_NULL;
}

// Question 09 : printEntries
// ##################################################
void printEntries(int inumber_dir);
void printEntries(int inumber_dir)
{
    struct inode_dir_s dir;
    int i;

    // lire directory
    read_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));
    assert(dir.magic == MAGIC);
    assert(dir.type == directory);
    for (i = 0; i < NB_ENTRIES; i++)
    {
        // entry trouvée
        if (strcmp(dir.entries[i].filename, "") != 0)
        {
            printf("fichier : %s ----> %d", dir.entries[i].filename, dir.entries[i].file_number);
        }
    }
    // Si je suis ici, c'est que je n'ai pas trouvé dans le premier bloc de type directory

    inumber_dir = dir.next;
    while (inumber_dir != BLOC_NULL)
    {
        // lire directory
        read_bloc_n(current_volume, inumber_dir, (char *)&dir, sizeof(struct inode_dir_s));
        assert(dir.magic == MAGIC);
        assert(dir.type == special);

        for (i = 0; i < NB_ENTRIES; i++)
        {
            // entry trouvée
            if (strcmp(dir.entries[i].filename, "") != 0)
            {
                printf("fichier : %s ----> %d", dir.entries[i].filename, dir.entries[i].file_number);
            }
        }
    }
}

// ################################################################################################################
// ################################################################################################################
// ################################################################################################################
// ################################################################################################################
// ################################################################################################################

// Ordonnanceur temps réel
void timer_handler(); // handler de l'interruption clock
void empty_it();

// Question 10 : get_current_time
// ##########################################################################
int get_current_time()
{
    int time;
    time = _in(TIMER_CLOCK);
    return time;
}

// Question 11 : init_timer
// ##########################################################################
void init_timer()
{
    int i;
    for (i = 0; i < 16; i++)
    {
        IRQVECTOR[i] = empty_it;
    }
    IRQVECTOR[TIMER_IRQ] = timer_handler;
    _out(TIMER_PARAM, 128 + 64 + 32 + 8);
    _out(TIMER_ALARM, 0xFFFFFFFE);
    _mask(1);
    yield();
}

// Question 12 : changer la structure du contexte
typedef void(func_t)(void *);
enum ctx_state_e
{
    ACTIVABLE,
    READY,
    TERMINATED
};
struct ctx_s
{
    enum ctx_state_e state;
    func_t *f;
    void *args;
    void *esp;
    void *ebp;
    unsigned char *stack;
    struct ctx_s *ctx_next;
    unsigned int period;
    unsigned int total_budget;
    unsigned remaining_budget;
    unsigned int next_deadline;
    struct ctx_s *waiting_next; // TODO : verifier
};

// Question 13 : modification create_ctx
// ##################################################
int create_ctx(int stack_size, func_t f, void *args, unsigned int period, unsigned total_budget)
{
    struct ctx_s *ctx;
    ctx = (struct ctx_s *)malloc(sizeof(struct ctx_s));
    assert(ctx);
    ctx->state = ACTIVABLE;
    ctx->f = f;
    ctx->args = args;
    ctx->stack = malloc(stack_size);
    assert(ctx->stack);
    ctx->period = period;
    ctx->total_budget = total_budget;
    ctx->remaining_budget = total_budget;
    ctx->next_deadline = get_current_time() + period;
}

struct ctx_s *current_ctx;

// Question 14 : modif de yield()
// #####################################################
void yield()
{
    // SI j'ai un ctx courant
    if (current_ctx)
    {
        // SELECTIONNER premier de la liste (echeance la + courte)
        current_ctx->remaining_budget--;
         
    }
    else
    {
    }
}