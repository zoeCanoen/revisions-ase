#include <string.h>

#define FILE_NAME_SIZE 32
#define BLOCK_SIZE 255

enum file_type_e{
    DIRECTORY,
    SPECIAL
};

struct file_entry {
    int inumber;
    char name[ FILE_NAME_SIZE ];
};

int current_volume;

#define ENTRIES_SIZE (BLOCK_SIZE - sizeof( struct inode_dir_s ) ) / sizeof( struct file_entry )
#define INODE_MAGIC 0xC0FFEE
#define BLOCK_NULL 0

struct inode_dir_s {

    unsigned int magic;
    enum file_type_e type;
    int indirect;
    struct file_entry entries[ 0 ];

};

int createDirectory(){

    struct inode_dir_s* dir = malloc( BLOCK_SIZE );
    int block;

    dir->magic = INODE_MAGIC;
    dir->type = DIRECTORY;
    dir->indirect = BLOCK_NULL;

    memset( dir->entries, 0, ENTRIES_SIZE * sizeof( struct file_entry ) );
    
    block = new_block();
    assert( block != BLOCK_NULL );

    write_bloc_n( current_volume, block, dir, BLOCK_SIZE );

    free( dir );
    return block;
}

void removeDirectory( int inumber_dir ){

    struct inode_dir_s* dir;
    assert( inumber_dir != BLOCK_NULL );

    dir = malloc( BLOCK_SIZE );
    read_bloc_n( current_volume, inumber_dir, dir, BLOCK_SIZE );

    assert( dir->magic == INODE_MAGIC );
    assert( dir->type == DIRECTORY || dir->type == SPECIAL );
    
    if( dir->indirect != BLOCK_NULL ){
        removeDirectory( dir->indirect );
    }

    free_bloc( inumber_dir );
    free( dir );
}

void addEntry( int inumber_dir, char* filename, int file_inumber ){

    struct inode_dir_s *dir, *indirect_dir;
    unsigned int i;
    assert( inumber_dir != BLOCK_NULL );

    dir = malloc( BLOCK_SIZE );
    read_bloc_n( current_volume, inumber_dir, dir, BLOCK_SIZE );

    assert( dir->magic == INODE_MAGIC );
    assert( dir->type == DIRECTORY || dir->type == SPECIAL );

    for( i = 0; i < ENTRIES_SIZE; i++ ){

        if( strcmp( dir->entries[ i ].name, "") == 0 ){

            strcpy( dir->entries[ i ].name, filename );
            dir->entries[ i ].inumber = file_inumber;
            write_bloc_n( current_volume, inumber_dir, dir, BLOCK_SIZE );
            
            return;
        }

    }

    if( dir->indirect == BLOCK_NULL ){
        dir->indirect = createDirectory();

        indirect_dir = malloc( BLOCK_SIZE );
        read_bloc_n( current_volume, dir->indirect, indirect_dir, BLOCK_SIZE );
        indirect_dir->type = SPECIAL;
        write_bloc_n( current_volume, dir->indirect, indirect_dir, BLOCK_SIZE );

        write_bloc_n( current_volume, inumber_dir, dir, BLOCK_SIZE );
    }

    addEntry( dir->indirect, filename, file_inumber );
}

int rmEntry( int inumber_dir, char* filename ){

    struct inode_dir_s *dir, *indirect_dir;
    unsigned int i;
    int block;
    assert( inumber_dir != BLOCK_NULL );

    dir = malloc( BLOCK_SIZE );
    read_bloc_n( current_volume, inumber_dir, dir, BLOCK_SIZE );

    assert( dir->magic == INODE_MAGIC );
    assert( dir->type == DIRECTORY || dir->type == SPECIAL );

    for( i = 0; i < ENTRIES_SIZE; i++ ){

        if( strcmp( dir->entries[ i ].name, filename ) == 0 ){

            strcpy( dir->entries[ i ].name, "" );
            block = dir->entries[ i ].inumber;
            dir->entries[ i ].inumber = BLOCK_NULL;
            write_bloc_n( current_volume, inumber_dir, dir, BLOCK_SIZE );
            
            return block;
        }
    }

    if( dir->indirect != BLOCK_NULL ){
        return rmEntry( dir->indirect, filename );
    }

    free( dir );
    return 0;
}

int findEntry( int inumber_dir, char* filename ){

    struct inode_dir_s *dir, *indirect_dir;
    unsigned int i;
    int block;
    assert( inumber_dir != BLOCK_NULL );

    dir = malloc( BLOCK_SIZE );
    read_bloc_n( current_volume, inumber_dir, dir, BLOCK_SIZE );

    assert( dir->magic == INODE_MAGIC );
    assert( dir->type == DIRECTORY || dir->type == SPECIAL );

    for( i = 0; i < ENTRIES_SIZE; i++ ){

        if( strcmp( dir->entries[ i ].name, filename ) == 0 ){
            return dir->entries[ i ].inumber;
        }
    }

    if( dir->indirect != BLOCK_NULL ){
        return findEntry( dir->indirect, filename );
    }

    free( dir );
    return 0;
}